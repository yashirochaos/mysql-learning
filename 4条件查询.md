

# 条件查询

## WHERE子句

该语句可以实现按照一定的条件进行查询功能

语法
```sql
SELECT <字段列表>
FROM  <表名>
WHERE <条件表达式>
```

以下是常见的比较运算符


|运算符|说明|
|:-:    |   :-:|
|=|等于|
|>|大于|
|<|小于|
|>=|大于等于|
|<=|小于等于|
|!>|不大于|
|!<|不小于|
|<>或！=|等于|


## 使用比较与运算符限制查询结果

1. 使用=查询数据

如果相同条件满足，将该记录显示

```sql
SELECT
	*
FROM
	goods
WHERE
	id = 22;
```

2. 使用>查询数据

大于所指定条件的数据

```sql
SELECT
	id,
	`name`,
	views_count
FROM
	goods
WHERE
	views_count > 0
```

3. 使用<查询数据

查询小于条件的数据，注意表中的数据必须小于所指定的条件。

```sql
SELECT
	id,
	`name`,
	views_count
FROM
	goods
WHERE
	views_count < 16
```

4. >=查询

```sql
SELECT
	id,
	`name`,
	views_count
FROM
	goods
WHERE
	views_count >= 16
```

5. <=查询

```sql
SELECT
	id,
	`name`,
	views_count
FROM
	goods
WHERE
	views_count <= 16
```

6. !=查询

```sql
SELECT
	id,
	`name`,
	original_price
FROM
	goods
WHERE
	original_price != 2000
```