# 使用IN操作符过滤数据

在对表进行数据查询的时候，有时候需要查询出多个条件中满足一个条件的数据，这种情况可以使用OR运算符，但是对于较多的条件来说使用OR运算符并不方便。例如在表中查询出省份为“吉林省”等数据，此时可以用IN操作符代替OR运算符完成查询任务。


## 1. 使用IN查询数据

使用IN查询操作符可以判断某个字段的值是否在指定的集合中，如果值在集合中，则满足查询条件，该记录将被查询出来，如果不在集合中，则满足查询条件
格式如下：

```sql
SELECT column_name
FROM table_name
WHERE column_name IN (value1,vaule2,....)
```

参数说明

- cloumn_name : 表示需要查询的列名

- table_name : 表示需要查询的表名

- cloumn_name: 表示需要指定查询条件的列

- value : 表示值列表

```sql
SELECT id,`name`,current_price  FROM goods
WHERE id IN (22,44,55);
```

在IN操作符后的值列表中不但可以使用数值类型数据，还可以使用字符类型数据

```sql
SELECT id,`name`,current_price  FROM goods
WHERE `name` IN ('三星','西门子');
```

## 2. 在IN中使用计算表达式

在IN操作符后的值列表中还可以使用算数表达式

```sql
SELECT id,`name`,current_price  FROM goods
WHERE current_price IN (3799-2400,2000+100);
```

## 3. 在IN中使用列进行查询

在使用IN查询的时候，不但可以使用数值类型和字符类型进行值查询，还可以使用列名作为值进行查询

```sql
SELECT id,`name`,original_price,current_price  FROM goods
WHERE 1399  IN (original_price,current_price);
```

## 4. 使用NOT IN查询数据

NOT IN 可以查询出给定条件的以为的数据 格式如下：

```sql
SELECT column_name
FROM table_name
WHRER column_name NOT IN (value1,value2.....)
```
参数说明
- column_name : 表示要查询的列的列名

- table_name : 表示需要查询的表名

- column_name : 表示需要指定的查询条件的列

- value : 表示值的列表

```sql
SELECT id,`name`,original_price,current_price  FROM goods
WHERE 1399 NOT IN (original_price,current_price);
```

## 5. 使用NOT IN查询后两行的数据

使用NOT IN 查询后两行的数据，前提条件是需要知道表中共有多少行的数据，可以先用TOP将前几行查询出来，然后用NOT IN 查询后两行

```
SELECT id,`name`,original_price,current_price  FROM goods
WHERE id NOT IN (SELECT TOP 8 id FROM goods);
```

